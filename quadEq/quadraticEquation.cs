﻿using System;

namespace quadEq
{
    public class quadraticEquation
    {
        double x1, x2;

         public double[] Solve(double a, double b, double c, double eps = 1e-5){
            if (Math.Abs(a) < eps){
                throw new ArgumentException("Argument a is zero!");
            }
            else{
                double D = b * b - 4 * a * c;
                if (Math.Abs(D) < eps){
                    x1 = -b / 2;
                    x2 = -b / 2;
                    return new double[2] {x1, x2};
                }
                else if (D > eps) {
                    x1 = (-b + Math.Sqrt(D)) / 2;
                    x2 = (-b - Math.Sqrt(D)) / 2;
                    return new double[2] {x1, x2};
                }
                else{
                    return new double[0];
                }
            }
        }
    }
}
                
            
        
        
