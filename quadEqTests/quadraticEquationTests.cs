using System;
using Xunit;
using quadEq;

namespace quadEqTests
{
    public class quadraticEquationTests
    {
        const double eps = 1e-5;
        
        [Fact]
        public void OneRootOfSecondMultiplicity()
        {
            quadraticEquation roots = new quadraticEquation();

            var result = roots.Solve(1, -2, 1, eps);

            Assert.True(EquationShouldHasRoots(1, 1, result, eps));
        }

        [Fact]
        public void TwoRootsOfFirstMultiplicity()
        {
            quadraticEquation roots = new quadraticEquation();

            var result = roots.Solve(1, 0, -1, eps);

            Assert.True(EquationShouldHasRoots(1, -1, result, eps));
        }

        [Fact]
        public void NoRoots()
        {
            quadraticEquation roots = new quadraticEquation();

            var result = roots.Solve(1, 0, 1, eps);

            Assert.Equal(0, result.Length);
        }

        [Fact]
        public void AIsZero()
        {
            Assert.Throws<ArgumentException>(() => (new quadraticEquation()).Solve(1e-6, 1, -1, eps));
        }

        private bool EquationShouldHasRoots(double expected1, double expected2, double[] roots, double eps)
        {
            return Math.Abs(roots[0] - expected1) < 1e-5 && Math.Abs(roots[1] - expected2) < eps;
        }
    }
}
